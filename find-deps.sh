#!/bin/bash

# find-deps.sh -- find packages depending of a specific package
#
# This file is part of the forensics-all and is useful for debugs.
#
# Copyright 2022-2024 Joao Eriberto Mota Filho <eriberto@debian.org>
#
# You can use this program under the BSD-3-Clause conditions.

####################
### Main program ###
####################

### Is there $1?

[ "$1" ] || { \
    echo -e "\nfind-deps.sh -- find packages depending of a specific package\n"; \
    echo -e "You must provide a dependency name. I will search for packages using this\ndependency. My source is the file list-of-packages and I will use grep.\n\nAborting.\n"; \
    exit 1; }
[ -e list-of-packages ] || { echo -e "\nI can't find list-of-packages. Aborting.\n"; exit 1; }

### Go!

LISTFILES=$(cat list-of-packages | egrep -v "^#" | egrep '(FD|FG|FO)' | cut -d" " -f1 | tr '|' ' ')

# Pre-checking

apt-get install --no-install-recommends -s $LISTFILES | grep '^Inst' | grep $1 > /dev/null || \
    { echo -e "\nNot found packages depending of $1.\nNote this script only checks packages set as FD, FG and FO.\n"; exit 0; }

# Package by package check

for i in $(echo $LISTFILES)
do
    echo $i; apt-get install --no-install-recommends -s $i | grep '^Inst' | grep $1 && \
    { echo -e "\nBreaking...\n\nI found a package depending of $1: $i.\nPlease, fix list-of-files and run find-deps.sh again.\n"; break; }
done

exit 0

#!/bin/bash

# find-build-deps.sh -- find packages build depending of a specific package
#
# This file fully based in find-deps.sh and it is part of the
# forensics-all. It is useful for debugs.
#
# Copyright 2022-2024 Joao Eriberto Mota Filho <eriberto@debian.org>
#
# You can use this program under the BSD-3-Clause conditions.

####################
### Main program ###
####################

### Is there $1?

[ "$1" ] || { \
    echo -e "\nfind-build-deps.sh -- find packages build depending of a specific package\n"; \
    echo -e "You must provide a build dependency name. I will search for packages using this\nbuild dependency. My source is the file list-of-packages and I will use grep.\n\nAborting.\n"; \
    exit 1; }
[ -e list-of-packages ] || { echo -e "\nI can't find list-of-packages. Aborting.\n"; exit 1; }

### Go!

LISTFILES=$(cat list-of-packages | egrep -v "^#" | egrep '(FD|FG|FO)' | cut -d" " -f1 | tr '|' ' ')

for i in $(echo $LISTFILES)
do
    echo $i; apt-get build-dep -s $i | grep '^Inst' | grep $1 && \
    { echo -e "\nBreaking...\n\nI found a package build depending of $1: $i.\nPlease, fix list-of-files and run find-build-deps.sh again."; break; }
done

exit 0

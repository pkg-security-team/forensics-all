#!/bin/bash

# synopsis.sh -- show short description for packages inside list-of-packages
#
# This file is part of the forensics-all.
#
# Copyright 2018 Joao Eriberto Mota Filho <eriberto@debian.org>
#
# You can use this program under the BSD-3-Clause conditions.

####################
### Main program ###
####################

### Initial check. We are in right place?

[ -e list-of-packages ] || { echo "I can't find list-of-packages file. Aborting."; exit 1; }

### Go!

# Help
function help() {
    echo -e "\nUsage: synopsis.sh [OPTION]"
    echo -e "\n-h  Show help."
    echo "FD  Show short description for packages in Depends field (forensics-all)."
    echo "FR  Show short description for packages in Recommends field (forensics-all)."
    echo "FG  Show short description for packages in Depends field (forensics-all-gui)."
    echo "FX  Show short description for packages in Recommends field (forensics-all-gui)."
    echo "<empty>  Show short description for all packages in list-of-packages."
    echo " "
    exit 1
}

[ "$1" != "FD" -a "$1" != "FR" -a "$1" != "FG" -a "$1" != "FX" -a ! -z "$1" ] && help

# Operation mode
[ "$1" = "FD" -o "$1" = "FR" -o "$1" = "FG" -o "$1" = "FX" ] && INFO="$1" || INFO=" "

# List of packages in local list-of-packages file.
FILES=$(cat list-of-packages | egrep "$INFO" | cut -d" " -f1  | egrep '^[a-z0-9]')

# The synopsis...
function error() {
    echo -e "\nERROR: package $i not found. Aborting."
    echo -e "\nERROR: package $i not found. Aborting." > synopsis.ERROR
    exit 1
}

for i in $FILES
do
    LANG=C apt-cache search --names-only $i | egrep "^$i -" || error
done

exit 0

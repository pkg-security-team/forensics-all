#!/bin/bash

# gen-control.sh -- generates debian/control and debian/README.Debian files
#
# This file is part of the forensics-all.
#
# Copyright 2018 Joao Eriberto Mota Filho <eriberto@debian.org>
#
# You can use this program under the BSD-3-Clause conditions.

#################
#################
### Variables ###
#################
#################

[ -e variables ] || { echo "I can't find variables file. Aborting."; exit 1; }

source variables

####################
####################
### Main program ###
####################
####################

### Initial check

[ -e /usr/bin/wrap-and-sort ] || { echo "ERROR: wrap-and-sort command not found"; exit 1; }

[ -e list-of-packages ] || { echo "I can't find list-of-packages file. Aborting."; exit 1; }

function synopsis-check() {
    [ -e synopsis.ERROR ] && { echo -e "\n\nERROR: I found synopsis.ERROR file. Aborting.\n"; exit 1; }
}

synopsis-check

[ -d build ] || mkdir build


### Initial message

echo -e "\ngen-control.sh -- generates debian/control file"
echo " "
echo "The 'search-for-new.sh' should be used before this script. "
echo " "
echo -e "You can edit the \e[5mvariables\e[25m file to exclude packages."
echo " "
echo "Press ENTER to continue or Ctrl-C to abort."
read NOTHING

#################
# forensics-all #
#################

### Showing...

# Depends

echo '  __                          _                      _ _ '
echo ' / _| ___  _ __ ___ _ __  ___(_) ___ ___        __ _| | |'
echo '| |_ / _ \|  __/ _ \  _ \/ __| |/ __/ __|_____ / _  | | |'
echo '|  _| (_) | | |  __/ | | \__ \ | (__\__ \_____| (_| | | |'
echo '|_|  \___/|_|  \___|_| |_|___/_|\___|___/      \__,_|_|_|'

echo -e "\n\n-------------------------------------------------------------------------"
echo "The packages to be put in DEPENDS field (debian/control) are listed below"
echo "-------------------------------------------------------------------------"
echo " "
cat list-of-packages | egrep '(FD|FO)' | egrep '^[a-z]' | cut -d" " -f1 > build/depends
cat build/depends | xargs
echo " "
echo "TOTAL: $(cat build/depends | wc -l) packages."
echo "Press ENTER to continue..."
read NOTHING

# Suggests

cat list-of-packages | egrep FS | egrep -v "^forensics-all FS$" | egrep '^[a-z]' | \
    cut -d" " -f1 > build/suggests

echo -e "\n\n----------------------------------------------------"
echo "For SUGGESTS field, there are the following packages"
echo -e "----------------------------------------------------\n"

if [ -s build/suggests ]
then
    cat build/suggests | xargs
else
    echo "No packages for this field."
fi

# Recommends

cat list-of-packages | egrep FR | egrep '^[a-z]' | cut -d" " -f1 > build/recommends

echo -e "\n\n------------------------------------------------------"
echo "For RECOMMENDS field, there are the following packages"
echo -e "------------------------------------------------------\n"

if [ -s build/recommends ]
then
    cat build/recommends | xargs
else
    echo "No packages for this field."
fi

# Ignored

cat list-of-packages | egrep FI'( |$)' | egrep '^[a-z]' | cut -d" " -f1 > build/ignored

echo -e "\n\n--------------------------------------"
echo "The following packages will be IGNORED"
echo -e "--------------------------------------\n"

if [ -s build/ignored ]
then
    cat build/ignored | xargs
else
    echo -e "No packages to be ignored."
fi

# Waitting

echo -e "\n\nPress ENTER to continue or Ctrl-C to abort."
echo " "
read NOTHING

### Build the partial debian/control file

cat templates/control.part1 | sed "s/SV-VALUE/$STD_VER/" | \
    sed "s/DH-VALUE/$DH_LEVEL/" > control.NEW

# Suggests
if [ -s build/suggests ]
then
    echo "Suggests: $(cat build/suggests | xargs | sed 's/ /, /g' | \
    sed 's/ /\n/g')" | sed 's/^/          /' | \
    sed 's/ \+Suggests:/Suggests:/' >> control.NEW
fi

# Recommends
if [ -s build/recommends ]
then
    echo "Recommends: $(cat build/recommends | xargs | sed 's/ /, /g' | \
    sed 's/ /\n/g')" | sed 's/^/          /' | \
    sed 's/ \+Recommends:/Recommends:/' >> control.NEW
fi

# Depends
echo "Depends: $(cat build/depends | xargs | sed 's/ /, /g' | \
   sed 's/$/,/' | sed 's/ /\n/g')" | sed 's/^/         /' | \
   sed 's/ \+Depends:/Depends:/' >> control.NEW

# Final part
cat templates/control.part2 >> control.NEW

cat build/depends | xargs | sed 's/ /, /g' | sed 's/,$/./' | fold -sw 69 | \
    sed 's/^/   /' | sed 's/|/ or /g' | sed 's/ $//' >> control.NEW

cat templates/control.part3 >> control.NEW



echo '  __                          _                      _ _                   _ '
echo ' / _| ___  _ __ ___ _ __  ___(_) ___ ___        __ _| | |       __ _ _   _(_)'
echo '| |_ / _ \|  __/ _ \  _ \/ __| |/ __/ __|_____ / _  | | |_____ / _` | | | | |'
echo '|  _| (_) | | |  __/ | | \__ \ | (__\__ \_____| (_| | | |_____| (_| | |_| | |'
echo '|_|  \___/|_|  \___|_| |_|___/_|\___|___/      \__,_|_|_|      \__, |\__,_|_|'
echo '                                                               |___/         '

echo -e "\n\n-------------------------------------------------------------------------"
echo "The packages to be put in DEPENDS field (debian/control) are listed below"
echo "-------------------------------------------------------------------------"
echo " "
cat list-of-packages | egrep 'FG' | egrep '^[a-z]' | cut -d" " -f1 > build/depends2
cat build/depends2 | xargs
echo " "
echo "TOTAL: $(cat build/depends2 | wc -l) packages."
echo "Press ENTER to continue..."
read NOTHING

# Header of package
cat templates/control.part4 >> control.NEW

# Suggests
cat list-of-packages | egrep FS | egrep -v "^forensics-all-gui FS$" | egrep '^[a-z]' | \
    cut -d" " -f1 > build/suggests2

echo -e "\n\n----------------------------------------------------"
echo "For SUGGESTS field, there are the following packages"
echo -e "----------------------------------------------------\n"

if [ -s build/suggests2 ]
then
    cat build/suggests2 | xargs
else
    echo "No packages for this field."
fi


# Recommends

cat list-of-packages | egrep FX | egrep '^[a-z]' | cut -d" " -f1 > build/recommends2

echo -e "\n\n------------------------------------------------------"
echo "For RECOMMENDS field, there are the following packages"
echo -e "------------------------------------------------------\n"

if [ -s build/recommends2 ]
then
    cat build/recommends2 | xargs
else
    echo "No packages for this field."
fi

# Waitting

echo -e "\n\nPress ENTER to continue or Ctrl-C to abort."
echo " "
read NOTHING

# Suggests
if [ -s build/suggests2 ]
then
    echo "Suggests: $(cat build/suggests2 | xargs | sed 's/ /, /g' | \
    sed 's/ /\n/g')" | sed 's/^/          /' | \
    sed 's/ \+Suggests:/Suggests:/' >> control.NEW
fi

# Recommends
if [ -s build/recommends2 ]
then
    echo "Recommends: $(cat build/recommends2 | xargs | sed 's/ /, /g' | \
    sed 's/ /\n/g')" | sed 's/^/          /' | \
    sed 's/ \+Recommends:/Recommends:/' >> control.NEW
fi

# Depends
echo "Depends: $(cat build/depends2 | xargs | sed 's/ /, /g' | \
   sed 's/$/,/' | sed 's/ /\n/g')" | sed 's/^/         /' | \
   sed 's/ \+Depends:/Depends:/' >> control.NEW

# Final part
cat templates/control.part5 >> control.NEW

cat build/depends2 | xargs | sed 's/ /, /g' | sed 's/,$/./' | fold -sw 69 | \
    sed 's/^/   /' | sed 's/|/ or /g' | sed 's/ $//' >> control.NEW

cat templates/control.part3 >> control.NEW

wrap-and-sort -f ./control.NEW

echo -e "\n\n---> Created control.NEW..."

################################
################################
### Create README.Debian.NEW ###
################################
################################

# forensics-all.README.Debian
echo -e "\n\n---> Creating forensics-all.README.Debian.NEW..."
echo "forensics-all for Debian" > forensics-all.README.Debian.NEW
echo "------------------------" >> forensics-all.README.Debian.NEW
echo -e "\nLIST OF PACKAGES INSTALLED BY forensics-all" >> forensics-all.README.Debian.NEW
echo -e "\n\nCOMMON PACKAGES (available for all machines)\n"  >> forensics-all.README.Debian.NEW
./synopsis.sh FD >> forensics-all.README.Debian.NEW
echo -e "\nRECOMMENDED PACKAGES (available for some architectures only)\n" >> forensics-all.README.Debian.NEW

TEST=$(cat list-of-packages | grep FR)
if [ "$TEST" ]
then
    ./synopsis.sh FR >> forensics-all.README.Debian.NEW
else
    echo "No packages for this field at this moment." >> forensics-all.README.Debian.NEW
fi

synopsis-check

echo -e "\n" >> forensics-all.README.Debian.NEW
echo " -- $DEBFULLNAME <$DEBEMAIL>  $(LANG=C date -R)" >> forensics-all.README.Debian.NEW

# forensics-all-gui.README.Debian
echo -e "\n\n---> Creating forensics-all-gui.README.Debian.NEW..."
echo "forensics-all-gui for Debian" > forensics-all-gui.README.Debian.NEW
echo "----------------------------" >> forensics-all-gui.README.Debian.NEW
echo -e "\nLIST OF PACKAGES INSTALLED BY forensics-all-gui" >> forensics-all-gui.README.Debian.NEW
echo -e "\n\nCOMMON PACKAGES (available for all machines)\n"  >> forensics-all-gui.README.Debian.NEW
./synopsis.sh FG >> forensics-all-gui.README.Debian.NEW
echo -e "\nRECOMMENDED PACKAGES (available for some architectures only)\n" >> forensics-all-gui.README.Debian.NEW

TEST=$(cat list-of-packages | egrep " FX" | egrep "^[a-z0-9]")
if [ "$TEST" ]
then
    ./synopsis.sh FX >> forensics-all-gui.README.Debian.NEW
else
    echo "No packages for this field at this moment." >> forensics-all-gui.README.Debian.NEW
fi

synopsis-check

echo -e "\n" >> forensics-all-gui.README.Debian.NEW
echo " -- $DEBFULLNAME <$DEBEMAIL>  $(LANG=C date -R)" >> forensics-all-gui.README.Debian.NEW

### Final message

echo -e "\n\n\n"
echo "The control.NEW, forensics-all.README.Debian.NEW and"
echo "forensics-all-gui.README.Debian.NEW files are done."
echo "Please, remove the NEW suffixes and move to debian/"
echo "directory. You can use the move.sh script for this."
echo " "


### Remove the build directory

[ -d build ] && rm -rf build

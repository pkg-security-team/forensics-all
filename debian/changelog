forensics-all (3.58) unstable; urgency=medium

  * LICENSE: updated copyright years.
  * list-of-packages:
      - Changed from FD to FR:
          ~ brutespray. See #1090342.
          ~ exifprobe. See #1089549.
          ~ medusa. See #1090342.
  * debian/control: updated.
  * debian/copyright: updated packaging copyright years.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 09 Jan 2025 13:47:10 -0300

forensics-all (3.57) unstable; urgency=medium

  * list-of-packages: removed hydra-gtk. See #967532. Thanks to Jeremy
    Bícha <jeremy.bicha@canonical.com>. (Closes: #1081885)
  * debian/control: updated.
  * debian/forensics-all-gui.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 16 Sep 2024 00:27:20 -0300

forensics-all (3.56) unstable; urgency=medium

  * find-deps.sh: improved a message.
  * find-x11.sh: added a pre-checking to make the script running faster.
  * gen-control.sh: removed unnecessary text.
  * list-of-packages:
      - Added new packages:
        ~ FD: altdns, argon2, assetfinder, gpshell, graudit, readpe, sherlock,
              sploitscan, usbrip, waymore.
        ~ FR: exiflooter, paramspider.
        ~ FS: yubihsm-auth, yubihsm-connector, yubihsm-pkcs11, yubihsm-shell,
              yubihsm-wrap.
        ~ FG: zenmap.
        ~ FI: pev.
        ~ SS: greenbone-feed-sync, postgresql-16-pg-gvm, raven, reaver, shisa,
              shishi, shishi-kdc, slowloris, vpnc.
      - Removed no loger existent packages: bdfproxy, openvas, pg-gvm.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 26 Aug 2024 14:27:38 -0300

forensics-all (3.55) unstable; urgency=medium

  * find-deps.sh: added a pre-checking to make the script running faster.
  * list-of-packages: changeme and patator conflicts via dependencies and must
    be FS, instead of FR (or FD). This is important for forensics-full. Setting
    FS for both packages.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 20 Aug 2024 16:25:10 -0300

forensics-all (3.54) unstable; urgency=medium

  * find-build-deps.sh: created to search for packages with a specific build
    dependency. Similar to find-deps.sh.
  * find-deps.sh: added a tittle and reorganized the message about "providing
    a dependency name".
  * find-x11.sh: created to search for packages FD|FR|FS depending of
    x11-common. These packages should be referenced as FG|FX.
  * list-of-packages:
      - Solving a conflict:
          ~ changeme was set as FG and depends of python3-pysnmp-lextudio,
            python3-pysnmp-pyasn1 and python3-pysnmp-pysmi.
          ~ patator was set as FD and depends of python3-pysnmp4,
            python3-pyasn1 and python3-pysmi.
          ~ python3-pysnmp-lextudio and python3-pysnmp4 conflicts.
          ~ python3-pysnmp-pyasn1 and python3-pyasn1 conflicts.
          ~ python3-pysnmp-pysmi and python3-pysmi conflicts.
          ~ Consequently:
              ~ Set changeme as FX.
              ~ Set patator as FR.
      - Changed from FD to FR:
          - de4dot (not available at mips64el and riscv64).
          - Working on console, but installs x11-common:
              ~ dsniff
              ~ magicrescue
              ~ time-decode
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 19 Aug 2024 12:43:04 -0300

forensics-all (3.53) unstable; urgency=medium

  * list-of-packages:
      - Changed from FD to FR:
          ~ reglookup. See #1075442.
          ~ stegcracker. See #1075183.
          ~ steghide. See #1075183.
          ~ stegseek. See #1075183.
      - Changed from FR to FD:
          ~ dc3dd
          ~ dsniff
          ~ ewf-tools
          ~ grokevt
          ~ hashrat
          ~ nmap
          ~ pompem
          ~ scrounge-ntfs
          ~ sucrack
          ~ vinetto
          ~ wfuzz
      - Changed from FX to FG:
          ~ changeme
      - Updated bug number, keeping current state (FI or FR):
          ~ aircrack-ng. See #1051201 and #1071832.
          ~ airgraph-ng. See #1051201 and #1071832.
          ~ bully. See #1051201 and #1071832.
          ~ crack|crack-md5. See #1074890.
          ~ mdk3. See #1051201 and #1071832.
          ~ mdk4. See #1051201 and #1071832.
          ~ wifite. See #1051201 and #1071832.
  * variables: bumped STD_VER to 4.7.0.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 16 Aug 2024 13:38:35 -0300

forensics-all (3.52) unstable; urgency=medium

  * list-of-packages: changed from FD to FR:
      - grokevt, see #1065871.
      - pompem, see #1074500.
      - vinetto, see #1076036 (python3-setuptools).
      - wapiti, see #1072468.
      - wfuzz, see #1066009, #1067598.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 09 Jul 2024 18:40:52 -0300

forensics-all (3.51) unstable; urgency=medium

  * list-of-packages: changed dc3dd from FD to FR. See #1069507.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 03 May 2024 13:08:52 -0300

forensics-all (3.50) unstable; urgency=medium

  * list-of-packages:
      - Changed from FO to FI: crack. See #1066634.
      - Changed from FD to FR:
          ~ hashrat. See #1066510.
          ~ scrounge-ntfs. See #1066546.
          ~ sucrack. See #1066503.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 26 Apr 2024 16:34:02 -0300

forensics-all (3.49) unstable; urgency=medium

  * list-of-packages: changed stegsnow from FD to FR. See #1066597.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 23 Apr 2024 23:02:47 -0300

forensics-all (3.48) unstable; urgency=medium

  * LICENSE: updated copyright years.
  * list-of-packages:
      - Changed dsniff form FD to FR. This package is waiting for libtirpc
        migration to testing.
      - Removed radare2-cutter, no longer present in Debian.
  * debian/control: updated.
  * debian/copyright: updated packaging copyright years.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 23 Apr 2024 12:32:20 -0300

forensics-all (3.47) unstable; urgency=medium

  * find-deps.sh: added an extra grep to monitor
    'Inst' lines to avoid false positives.
  * list-of-packages:
      - Changed from FD to FR:
          ~ bully (see #1000021)
          ~ mdk3 (see #1000021)
          ~ mdk4 (see #1000021)
      - Changed from FG to FX:
          ~ airgraph-ng (see #1000021)
          ~ changeme (see #1000012, #1039613)

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 23 Jul 2023 23:05:29 -0300

forensics-all (3.46) unstable; urgency=medium

  * list-of-packages:
      - Added (new): stegseek
      - Changed from FD to FR:
          ~ aircrack-ng (see #1000021)
          ~ nmap (see #1039613)
          ~ wapiti (see #1032198)
      - Changed from FR to FD:
          ~ dislocker, hydra, yara
      - Changed from FX to FG: hydra-gtk
      - Kept in FR: wifite, now because #1000021
      - Removed: afl, afl++-clang and afl-clang
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 23 Jul 2023 09:19:07 -0300

forensics-all (3.45) unstable; urgency=medium

  * list-of-packages: moved wifite from FD to FR. See #1036809 and #1036591.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 31 May 2023 16:38:48 -0300

forensics-all (3.44) unstable; urgency=medium

  * list-of-packages:
      - Changed hydra from FD to FR. See #1032479.
      - Changed hydra-gtk from FG to FX. See #1032479.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 16 Mar 2023 08:04:52 -0300

forensics-all (3.43) unstable; urgency=medium

  * list-of-packages:
      - Added gobuster as SS.
      - Changed wapiti from FD to FR. See #1032198.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 01 Mar 2023 12:34:04 -0300

forensics-all (3.42) unstable; urgency=medium

  * list-of-packages:
      - Added arjun as SS.
      - Changed dislocker from FD to FR. See #1024589.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 09 Feb 2023 11:01:07 -0300

forensics-all (3.41) unstable; urgency=medium

  * LICENSE: updated copyright years.
  * list-of-packages:
      - Added notus-scanner and pg-gvm as SS.
      - Changed yara from FD to FR. See #1028868.
  * variables: bumped STD_VER to 4.6.2.
  * debian/control: updated.
  * debian/copyright: updated copyright years.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 22 Jan 2023 00:22:53 -0300

forensics-all (3.40) unstable; urgency=medium

  * list-of-packages: changed regripper from FI to FD.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/tests/control: set allow-stderr for the last test.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 12 Dec 2022 00:20:05 -0300

forensics-all (3.39) unstable; urgency=medium

  * list-of-packages:
      - Added a comment for plaso.
      - Fixed arp-scan (SS).
      - Moved binwalk and rfdump from FD to FG.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.
  * debian/tests/control: added two new tests.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 29 Nov 2022 02:14:49 -0300

forensics-all (3.38) unstable; urgency=medium

  * list-of-packages:
      - Added the following packages:
          ~ FD: cisco7crack, creddump7, fatcat, hcxkeys.
          ~ FG: unhide-gui.
          ~ FI: cryptsetup-nuke-password.
          ~ FR: bruteforce-luks.
          ~ SS: bettercap, bettercap-caplets, cloud-enum, dirsearch, gsad,
                sublist3r.
      - Changes:
          ~ From FR to FD: hashdeep, sleuthkit, wapiti.
          ~ From FI to FD: time-decode, wifite.
          ~ From SS to FD: hcxtools.
      - Removed (no longer in Debian): sandsifter, xprobe.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.
  * debian/rules:
      - Replaced override_dh_auto_build by execute_before_dh_auto_build.
      - Replaced override_dh_clean by execute_after_dh_clean.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 25 Nov 2022 15:42:54 -0300

forensics-all (3.37) unstable; urgency=medium

  * variables: set STD_VER to 4.6.1.
  * debian/control: updated.
  * debian/salsa-ci.yml: added a request for autopkgtest over release 'testing'.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 15 Aug 2022 17:37:18 -0300

forensics-all (3.36) unstable; urgency=medium

  * list-of-packages: changed wapiti from FD to FR. See #1011443.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 20 Jun 2022 12:53:07 -0300

forensics-all (3.35) unstable; urgency=medium

  * find-deps.sh: new script. It find packages depending of a specific package.
    This new script is useful for debugs.
  * list-of-packages: changed ewf-tools from FD to FR. See #936854 and #1006393.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 20 May 2022 13:51:41 -0300

forensics-all (3.34) unstable; urgency=medium

  * license: updated copyright years.
  * list-of-packages: changed sleuthkit and xmount from FD to FR. See #936854
    and #1006393.
  * debian/control: updated.
  * debian/copyright: updated copyright years.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 16 May 2022 23:16:50 -0300

forensics-all (3.33) unstable; urgency=medium

  * list-of-packages:
      - Added hcxdumptool, hcxtools and pocsuite3 as SS.
      - Added time-decode as FI (not in testing yet).
      - Changed wifite from FD to FI. See #997333.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 06 Dec 2021 21:34:02 -0300

forensics-all (3.32) unstable; urgency=medium

  * list-of-packages:
      - Added firewalk as SS.
      - Added regripper as FI because it is not available in testing yet.
      - Changed hashdeep from FD to FR. See #984166.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 29 Oct 2021 23:19:20 -0300

forensics-all (3.31) unstable; urgency=medium

  * list-of-packages:
      - Changed de4dot and dsniff from FR to FD.
      - Set aeskeyfind definitively as FR because it is available only for
        amd64 and i386.
  * variables: bumped STD_VER to 4.6.0.1.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 20 Sep 2021 11:15:13 -0300

forensics-all (3.30) unstable; urgency=medium

  * list-of-packages:
      - Changed aeskeyfind from FD to FR. See #989179.
      - Removed rekall-core, no longer available in Debian. See #980059.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 05 Jun 2021 12:37:16 -0300

forensics-all (3.29) unstable; urgency=medium

  * list-of-packages: added stegcracker as FD.
  * variables: fixed an IGNORE rule to avoid false positives.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 05 Feb 2021 23:03:45 -0300

forensics-all (3.28) unstable; urgency=medium

  * LICENSE: updated copyright years.
  * list-of-packages:
      - dsniff: moved from FD to FR. See #980588.
      - john: added as FD.
      - Removed:
          ~ greenbone-security-assistant, now in section contrib.
          ~ knocker and openvas-nasl: removed from Debian.
  * README: changed from apt-get to apt in instructions.
  * search-for-new.sh: changed from apt-get to apt in a message.
  * variables: bumped Standards-Version to 4.5.1.
  * debian/copyright: updated copyright years.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 05 Feb 2021 21:02:32 -0300

forensics-all (3.27) unstable; urgency=medium

  * list-of-packages:
      - Added as FD: bully, forensics-samples-all, forensics-samples-btrfs,
        forensics-samples-exfat, forensics-samples-ext2,
        forensics-samples-ext4, forensics-samples-files,
        forensics-samples-multiple, forensics-samples-ntfs,
        forensics-samples-tools and forensics-samples-vfat.
      - Added as SS: fierce.
      - Changed from FR to FD: wfuzz.
      - Removed (no longer in Debian): openvas-cli and openvas-manager.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 07 Nov 2020 20:49:33 -0300

forensics-all (3.26) unstable; urgency=medium

  * list-of-packages: changed from FR to FD: aircrack-ng, brutespray, de4dot,
    medusa, sleuthkit and tcpick.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/tests/control: improved the initial description to clarify the
    intent of the current CI tests. (Closes: #970949)

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 25 Sep 2020 23:00:51 -0300

forensics-all (3.25) unstable; urgency=medium

  * list-of-packages: changed wfuzz from FD to FR. See #969970.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 12 Sep 2020 11:54:55 -0300

forensics-all (3.24) unstable; urgency=medium

  * list-of-packages:
      - brutespray changed from FD to FR. brutespray depends of medusa.
        (See #957526)
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 22 Aug 2020 13:43:55 -0300

forensics-all (3.23) unstable; urgency=medium

  * Regenerated all README files.
  * Removed a wrong entry in last changelog.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 19 Aug 2020 14:46:22 -0300

forensics-all (3.22) unstable; urgency=medium

  * list-of-packages:
      - Added as SS: afl, afl++, afl++-clang, afl-clang, gvm, gvm-tools and
        gvmd.
      - Added snowdrop as FD.
      - Changed from FD to FR:
          ~ aircrack-ng, see #956990.
          ~ medusa, see #957526.
          ~ sleuthkit, see #957808.
          ~ tcpick, see #957860.
      - Changed from FR to FD: recoverjpeg.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 19 Aug 2020 13:05:36 -0300

forensics-all (3.21) unstable; urgency=medium

  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 02 Jul 2020 15:12:40 -0300

forensics-all (3.20) unstable; urgency=medium

  * list-of-packages: changed recoverjpeg from FD to FR (see #962755).

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 22 Jun 2020 14:58:36 -0300

forensics-all (3.19) unstable; urgency=medium

  * list-of-packages:
      - Added bruteforce-wallet as FD.
      - Added dnstwist, passwdqc and proxytunnel as SS.
      - Changed aircrack-ng, mdk3, mdk4 and wifite from FR to FD.
      - Changed airgraph-ng and changeme from FI to FG.
      - Set nmapsi4 as FX because it is available for amd64, arm64, armhf,
        i386 and mipsel. (See: #963154)
  * variables: bumped DH level to 13.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.
  * debian/tests/control: using simple messages via echo to force a full
    install of suites forensics-all and forensics-all-gui.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 19 Jun 2020 14:49:45 -0300

forensics-all (3.18) unstable; urgency=medium

  * list-of-packages:
      - Removed bbqsql and pyrit, no longer in Debian.
      - Removed neopi, no longer in Debian. Thanks to Sebastian Ramacher
        <sramacher@debian.org>. (Closes: #955777)

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 05 Apr 2020 00:00:36 -0300

forensics-all (3.17) unstable; urgency=medium

  * list-of-packages:
      - Added parsero as SS.
      - Removed volatility and volatility-tools, no longer present in Debian.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 24 Feb 2020 14:43:08 -0300

forensics-all (3.16) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * list-of-packages: fixed a mistake in changeme.
  * debian/control: updated.
  * debian/forensics-all-gui.README.Debian: updated.

  [ Samuel Henrique ]
  * Configure git-buildpackage for Debian.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 10 Feb 2020 21:43:08 -0300

forensics-all (3.15) unstable; urgency=medium

  * list-of-packages:
      - Added dnsmap and goldeneye as SS.
      - Set brutespray, smbmap and polenum as FD.
      - Set changeme as FI (see #950069).
      - Set volatility and volatility-tools as FI (see #950340).
  * Updated copyright years.
  * Variables: bumped Standards-Version to 4.5.0.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 05 Feb 2020 08:42:00 -0300

forensics-all (3.14) unstable; urgency=medium

  * list-of-packages:
      - Added ettercap-graphical and ettercap-text-only as SS.
      - Changed smbmap from FD to FI. See #936730.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 31 Dec 2019 00:27:41 -0300

forensics-all (3.13) unstable; urgency=medium

  * list-of-packages:
      - Changed mdk3 and mdk4 from FD to FR because it depends of
        aircrack-ng (see #936113)
      - Changed unhide from FR to FD because #945864 was closed.
      - Updated wifite status (but remains FR).
  * OLD/gen-control.sh: removed. The new gen-control.sh script is
    working fine.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 17 Dec 2019 20:49:10 -0300

forensics-all (3.12) unstable; urgency=medium

  * list-of-packages:
      - Added ewf-tools as FD.
      - Set goldeneye and unhide as FR. See #945648 and #945864.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 11 Dec 2019 17:39:16 -0300

forensics-all (3.11) unstable; urgency=medium

  * list-of-packages:
      - Added nasty as FD.
      - Changed aircrack-ng from FD to FR. See #936113.
      - Changed airgraph-ng from FG to FI. See #936113.
      - Changed brutespray from FD to FR. See #936243.
      - Removed bkhive because it is no longer in Debian.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 29 Nov 2019 20:17:44 -0300

forensics-all (3.10) unstable; urgency=medium

  * list-of-packages:
      - Added dnsenum as SS.
      - Changed polenum from FD to FI because it is uninstallable since
        python-colorama 0.4.1-1 revision.
      - Return forensic-artifacts to FD (from FR).
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 20 Nov 2019 19:15:39 -0300

forensics-all (3.9) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * list-of-packages:
      - Added new package: rekall-core (FR).
      - Removed no longer existent packages in Debian: md5deep (see #939241)
        and zenmap (see #885498).
  * templates/control.part1: added 'Rules-Requires-Root: no' to source stanza.
  * variables: bumped Standards-Version to 4.4.1.
  * debian/control: updated.
  * debian/copyright: added packaging rights for Samuel Henrique.
  * debian/forensics-all.README.Debian: updated.

  [ Samuel Henrique ]
  * Add salsa-ci.yml.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 12 Nov 2019 22:36:55 -0300

forensics-all (3.8) unstable; urgency=medium

  * list-of-packages:
      - Added new packages: de4dot (FR, see #934374), onesixtyone (SS)
        and pyrit (FR, see #937521).
      - Changed mdk4 status from FI to FD.
      - Removed no longer existent packages in Debian: grr-server and
        grr-client-templates-installer.
  * variables: bumped Standards-Version to 4.4.0.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 01 Sep 2019 12:13:07 -0300

forensics-all (3.7) unstable; urgency=medium

  * list-of-packages: set mdk4 as FI. See #921694.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 13 May 2019 20:56:13 -0300

forensics-all (3.6) unstable; urgency=medium

  * list-of-packages:
      - Added plaso as FR.
      - Added radare2-cutter as FX.
      - Added stegsnow as FD.
      - Set forensic-artifacts as FR (see #919974).
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 04 Feb 2019 22:59:28 -0200

forensics-all (3.5) unstable; urgency=medium

  * list-of-packages: set wifite as FR (see #919715).
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 19 Jan 2019 13:28:42 -0200

forensics-all (3.4) unstable; urgency=medium

  * gen-control.sh: fixed code to list recommends packages for
    forensics-all-gui.
  * list-of-packages:
      - Added arno-iptables-firewall and sandsifter as SS.
      - Changed wifite from FR to FD.
      - Changed guymager from FR to FX.
  * move.sh: added -f option to ignore missing files.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 18 Jan 2019 17:07:51 -0200

forensics-all (3.3) unstable; urgency=medium

  * LICENSE: updated copyright years.
  * list-of-packages: added pff-tools as FD.
  * variables: updated Standards-Version to 4.3.0 and DH level to 12.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - templates/control.part1: changed from 'debhelper' to 'debhelper-compat'
        in Build-Depends field.
  * debian/control: updated.
  * debian/copyright: updated copyright years.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.
  * debian/rules: added an override to remove 'build' directory.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 08 Jan 2019 13:57:25 -0200

forensics-all (3.2) unstable; urgency=medium

  * gen-control.sh: fixed block 'Ignored'.
  * list-of-packages:
      - Added smbmap.
      - Set 'changeme' as FG.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: updated.
  * debian/forensics-all-gui.README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 22 Dec 2018 23:41:45 -0200

forensics-all (3.1) unstable; urgency=medium

  * Upload to unstable.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 17 Dec 2018 22:07:35 -0200

forensics-all (3.0) experimental; urgency=medium

  * New package: forensics-all-gui. Consequently:
      - Adapted debian/rules.
      - Adapted gen-control.sh.
      - Adapted list-of-packages.
      - Adapted move.sh.
      - Adapted synopsis.sh.
      - Added two new templates: control.part4 and control.part5.
      - Removed debian/README.Debian because we need generate a README for
        each binary now.
      - Updated README.
  * search-for-new.sh: added FIXME procedure.
  * list-of-packages:
      - Added a comment for grr-client-templates-installer.
      - Added test-do-not-remove-it entry to be used as a trivial local test to
        confirm that search-for-new.sh is working.
      - Added two new packages: unhide and unhide.rb.
      - Changed wifite to FR FIXME.
      - Set greenbone-security-assistant as SS.
  * debian/control: updated.
  * debian/forensics-all.README.Debian: new README file.
  * debian/forensics-all-gui.README.Debian: new README file.
  * debian/tests/control:
      - Added a Depends field for forensics-all package.
      - Added a test for forensics-all-gui package.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 14 Dec 2018 11:46:44 -0200

forensics-all (2.3) unstable; urgency=medium

  * list-of-packages: set FR for ike-scan, currently not available for s390x.
  * move.sh: new script to move control.NEW and README.Debian.NEW to debian/.
  * README: updated to explain about move.sh.
  * debian/control: updated.
  * debian/README.Debian: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 11 Dec 2018 09:48:44 -0200

forensics-all (2.2) unstable; urgency=medium

  * list-of-packages:
      - Added new packages and full reviewed.
      - New suffix to be ignored: -data.
      - Set FR for ext3grep, gpart, guymager and hashcat, because they are not
        available in all official architectures.
  * gen-control.sh:
      - Added a new block to create a README.Debian.NEW file.
      - Removed a no longer needed message about 'run apt-get update'.
  * README: updated to tell about synopsis.sh.
  * search-for-new.sh:
      - Added comments in source code.
      - Added --debug option.
      - Added two new IGNORE variables.
      - Fixed the source code to work fine.
      - Improved the screen messages.
  * synopsis.sh: added to provide short description for all packages in
    list-of-packages and to check for non-existent packages.
  * variables: added two new IGNORE variables.
  * debian/control: updated.
  * debian/README.Debian: created to explain about each provided package.
  * debian/rules: added a test to make sure that some files were moved to
    debian/ directory.
  * debian/tests/control: removed deprecated restriction 'needs-recommends'.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 10 Dec 2018 19:06:41 -0200

forensics-all (2.1) unstable; urgency=medium

  * debian/tests/control: created to perform a trivial test.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 15 Oct 2018 10:50:03 -0300

forensics-all (2.0) unstable; urgency=medium

  * New source code for forensics-all because Debian Forensics Team was merged
    with Debian Security Tools Team. Changes:
      - Moved gen-control.sh to OLD directory to be kept as historical.
      - Created a new gen-control.sh file.
      - Created list-of-packages, search-for-new.sh, variables and README files.
      - Updated templates/control.part1 and templates/control.part2 files.
  * LICENSE: updated copyright years.
  * templates/*: updated templates to use the new source code and to be owned by
    Debian Security Tools Team.
  * debian/compat: migrated level to 11.
  * debian/control: updated from gen-control.sh script.
  * debian/copyright:
      - Updated packaging copyright years.
      - Using a secure copyright format in URI.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 30 Sep 2018 21:08:54 -0300

forensics-all (1.7) unstable; urgency=medium

  * gen-control.sh (source code changes):
      - Added new variables to:
          ~ Declare the Standards Version.
          ~ Declare suggested packages.
          ~ Declare recommended packages.
          ~ Set logical or for packages (e.g. foo|bar).
      - Updated the upstream copyright years.
  * gen-control.sh (configuration changes):
      - Bumped Standards-Version to 4.1.2.
      - Moved grr-client-templates-installer and grr-server to Suggests field.
        Thanks to Axel Beckert <abe@debian.org>. (Closes: #867860)
      - Moved outguess to Recommends field. (see: #882538)
      - Re-added md5deep to EXCLUDE field because hashdeep source still
        providing it.
  * LICENSE: updated the copyright years.
  * templates/control.part1:
      - Changed Standards-Version field to be written by gen-control.sh.
      - Removed no longer needed Suggests field. The gen-control.sh script will
        provide it.
  * debian/copyright: updated the copyright years.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 06 Dec 2017 16:49:41 -0200

forensics-all (1.6) unstable; urgency=medium

  * New release to fix dependency conflict and add new packages.
    (Closes: #852846, LP: #1658728)
  * Updated my email address.
  * Updated years in all copyright notices.
  * gen-control.sh: removed md5deep from EXCLUDE, this package has been
    removed from the Debian archive.
  * templates/control.part1:
      - Bumped Standards-Version to 4.0.0.
  * debian/control:
      - Removed hashdeep from Recommends field, now it is no longer necessary.

 -- Giovani Augusto Ferreira <giovani@debian.org>  Sat, 08 Jul 2017 23:26:45 -0300

forensics-all (1.5) unstable; urgency=medium

  * gen-control.sh: added forensics-full to EXCLUDE variable. Thanks a lot to
    Bill Allombert <ballombe@debian.org>. (Closes: #849485)
  * templates/control.part1: added forensics-all to Suggests field.
  * debian/control: moved temporarily (by hand) hashdeep to Recommends field to
    avoid forensics-all to be removed from testing.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 27 Dec 2016 16:31:29 -0200

forensics-all (1.4) unstable; urgency=medium

  * New version providing steghide, now in forensics team.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Thu, 22 Dec 2016 17:10:26 -0200

forensics-all (1.3) unstable; urgency=medium

  * gen-control.sh:
      - Added forensics-extra and forensics-extra-gui to EXCLUDE variable.
      - Added '-common' to DEPENDS variable.
  * templates/control.part1:
      - Added a Suggests field to make available the packages forensics-extra
        and forensics-extra-gui.
      - Bumped DH level to 10.
      - Bumped Standards-Version to 3.9.8.
  * Updated years in all copyright notices.
  * debian/compat: bumped to 10.
  * debian/control: moved temporarily hashdeep to Recommends field to avoid
    forensics-all to be removed from testing.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 02 Dec 2016 09:37:04 -0200

forensics-all (1.2) unstable; urgency=medium

  * gen-control.sh: removed guymager of EXCLUDE, because the package
    returned to Testing release.
  * templates/control.part1:
      - Changed from cgit to git in Vcs-Browser field.
      - Updated the Vcs-Git to use https:// instead of git://.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Wed, 23 Mar 2016 22:39:03 -0300

forensics-all (1.1) unstable; urgency=medium

  [ Giovani Augusto Ferreira ]

  * gen-control.sh: Included guymager in EXCLUDE temporarily until
    fix RC bugs in hdparm (see more in #725284 #779370 #779787 #780940)
  * templates/control.part1: set Standards-Version to 3.9.7.

  [ Joao Eriberto Mota Filho ]

  * debian/control: updated the Vcs-Git to use https:// instead of git://.
  * debian/copyright: updated some dates.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Sun, 21 Feb 2016 18:01:10 -0300

forensics-all (1.0) unstable; urgency=low

  * Initial release.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Mon, 21 Dec 2015 20:49:01 -0200

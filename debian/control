Source: forensics-all
Section: metapackages
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Giovani Augusto Ferreira <giovani@debian.org>,
           Joao Eriberto Mota Filho <eriberto@debian.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/pkg-security-team/forensics-all
Vcs-Git: https://salsa.debian.org/pkg-security-team/forensics-all.git

Package: forensics-all
Architecture: all
Suggests: changeme,
          forensics-all-gui,
          forensics-extra,
          forensics-extra-gui,
          forensics-full,
          forensics-samples-all,
          forensics-samples-btrfs,
          forensics-samples-exfat,
          forensics-samples-ext2,
          forensics-samples-ext4,
          forensics-samples-files,
          forensics-samples-multiple,
          forensics-samples-ntfs,
          forensics-samples-tools,
          forensics-samples-vfat,
          patator,
          yubihsm-auth,
          yubihsm-connector,
          yubihsm-pkcs11,
          yubihsm-shell,
          yubihsm-wrap
Recommends: aeskeyfind,
            aircrack-ng,
            bruteforce-luks,
            brutespray,
            bully,
            de4dot,
            dsniff,
            exiflooter,
            exifprobe,
            ext3grep,
            gpart,
            hashcat,
            ike-scan,
            magicrescue,
            mdk3,
            mdk4,
            medusa,
            paramspider,
            plaso,
            radare2,
            reglookup,
            stegcracker,
            steghide,
            stegseek,
            time-decode,
            wapiti,
            wifite,
            xmount
Depends: acct,
         aesfix,
         afflib-tools,
         altdns,
         argon2,
         assetfinder,
         braa,
         bruteforce-salted-openssl,
         bruteforce-wallet,
         btscanner,
         capstone-tool,
         ccrypt,
         cewl,
         chaosreader,
         chkrootkit,
         cisco7crack,
         cowpatty,
         creddump7,
         dc3dd,
         dirb,
         dislocker,
         dnsrecon,
         doona,
         ed2k-hash,
         ewf-tools,
         ext4magic,
         extundelete,
         fatcat,
         fcrackzip,
         forensic-artifacts,
         forensics-colorize,
         galleta,
         gpshell,
         graudit,
         grokevt,
         hashdeep,
         hashid,
         hashrat,
         hcxkeys,
         hcxtools,
         hydra,
         john,
         mac-robber,
         maskprocessor,
         masscan,
         memdump,
         metacam,
         mfcuk,
         mfoc,
         missidentify,
         myrescue,
         nasty,
         nbtscan,
         ncat,
         ncrack,
         ndiff,
         nmap,
         o-saft,
         ophcrack-cli,
         outguess,
         pasco,
         pff-tools,
         pipebench,
         pixiewps,
         pnscan,
         polenum,
         pompem,
         readpe,
         recoverdm,
         recoverjpeg,
         regripper,
         rephrase,
         rhash,
         rifiuti,
         rifiuti2,
         rkhunter,
         rsakeyfind,
         safecopy,
         samdump2,
         scalpel,
         scrounge-ntfs,
         shed,
         sherlock,
         sleuthkit,
         smbmap,
         snowdrop,
         sploitscan,
         ssdeep,
         ssldump,
         statsprocessor,
         stegsnow,
         sucrack,
         tableau-parm,
         tcpick,
         testssl.sh,
         undbx,
         unhide,
         unhide.rb,
         usbrip,
         vinetto,
         waymore,
         wfuzz,
         winregfs,
         wipe,
         yara,
         ${misc:Depends}
Description: Debian Forensics Environment - essential components (metapackage)
 This package provides the core components for a forensics environment.
 All here available tools are packaged by Debian Security Tools Team.
 This metapackage includes the most programs to data recovery, rootkit
 and exploit search, filesystems and memory analysis, image acquisition,
 volume inspection, special actions over the hardware and many other
 activities.
 .
 The following packages were included in this metapackage:
 .
   acct, aesfix, afflib-tools, altdns, argon2, assetfinder, braa,
   bruteforce-salted-openssl, bruteforce-wallet, btscanner,
   capstone-tool, ccrypt, cewl, chaosreader, chkrootkit, cisco7crack,
   cowpatty, creddump7, dc3dd, dirb, dislocker, dnsrecon, doona,
   ed2k-hash, ext4magic, extundelete, ewf-tools, fatcat, fcrackzip,
   forensic-artifacts, forensics-colorize, galleta, gpshell, graudit,
   grokevt, hashdeep, hashid, hashrat, hcxkeys, hcxtools, hydra, john,
   mac-robber, maskprocessor, masscan, memdump, metacam, mfcuk, mfoc,
   missidentify, myrescue, nasty, nbtscan, ncat, ncrack, ndiff, nmap,
   o-saft, ophcrack-cli, outguess, pasco, pff-tools, pipebench,
   pixiewps, pnscan, polenum, pompem, readpe, recoverdm, recoverjpeg,
   regripper, rephrase, rhash, rifiuti, rifiuti2, rkhunter, rsakeyfind,
   safecopy, samdump2, scalpel, scrounge-ntfs, shed, sherlock,
   sleuthkit, smbmap, snowdrop, sploitscan, ssdeep, ssldump,
   statsprocessor, stegsnow, sucrack, tableau-parm, tcpick, testssl.sh,
   undbx, unhide, unhide.rb, usbrip, vinetto, waymore, wfuzz, winregfs,
   wipe, yara
 .
 This metapackage is useful for pentesters, ethical hackers and forensics
 experts.

Package: forensics-all-gui
Architecture: all
Suggests: changeme,
          forensics-all,
          forensics-extra,
          forensics-extra-gui,
          forensics-full,
          forensics-samples-all,
          forensics-samples-btrfs,
          forensics-samples-exfat,
          forensics-samples-ext2,
          forensics-samples-ext4,
          forensics-samples-files,
          forensics-samples-multiple,
          forensics-samples-ntfs,
          forensics-samples-tools,
          forensics-samples-vfat,
          patator,
          yubihsm-auth,
          yubihsm-connector,
          yubihsm-pkcs11,
          yubihsm-shell,
          yubihsm-wrap
Recommends: airgraph-ng, guymager, nmapsi4
Depends: binwalk, ophcrack, rfdump, unhide-gui, zenmap, ${misc:Depends}
Description: Debian Forensics Environment - GUI components (metapackage)
 This package provides the core components for a GUI forensics environment.
 All here available tools are packaged by Debian Security Tools Team. This
 metapackage includes graphics programs, useful for some specific activities.
 .
 The following packages were included in this metapackage:
 .
   binwalk, ophcrack, rfdump, unhide-gui, zenmap
 .
 This metapackage is useful for pentesters, ethical hackers and forensics
 experts.
